<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Biosyn Chemicals Research Pvt Ltd</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- styles -->
   <?php include 'styles.php' ?>
</head>
<body>

    <!-- header -->
    <?php include 'header.php' ?>
    <!--/ header -->
    <!-- main -->
    <main class="subpage">

    <!-- sub page header -->
    <div class="subpage-header">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-6">
                    <article>
                        <h1>Careers</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>                                
                                <li class="breadcrumb-item active" aria-current="page">Careers</li>
                            </ol>
                        </nav>
                    </article>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </div>
    <!--/ sub page header -->

    <!-- sub page body -->
    <div class="subpage-body">
        <!-- container -->
        <div class="container">          

           <!-- row -->
           <div class="row">
               <!-- col -->
               <div class="col-lg-8 align-self-center">
                    <!-- row -->
                    <div class="row justify-content-center">
                        <!-- col -->
                        <div class="col-lg-6 text-center">
                            <h2 class="section-title aos-item" data-aos="fade-up">About  <span>working here</span></h2>
                            <p class="text-center pb-0 aos-item" data-aos="fade-down">Send out from the pack with your talent Send your Resume to <a data-aos="fade-up" href="javascript:void(0)" class="forange fbold aos-item">career@biosyn.in</a></p>                   
                        </div>
                        <!--/ col -->              
                    </div>
                    <!--/ row -->  
                    <!-- row -->
                    <div class="row py-md-5 justify-content-center">
                        <div class="col-lg-12 text-center">
                            <p class="text-center aos-item" data-aos="fade-up">We believe in employees driving the success of BRC. Join us and discover a work experience where diverse and innovative ideas are met with enthusiasm and where you can learn and grow to your full potential. We are looking for individuals who enjoy a fast-paced, collaborative environment, and who have a passion and commitment at work.</p>
                        </div>

                    </div>
                    <!--/ row -->
               </div>
               <!--/col -->

               <!-- col -->
               <div class="col-lg-4 aos-item" data-aos="fade-up">
                    <h2 class="section-title">Send   <span>Resume</span></h2>
                    <!-- form -->
                    <form>
                        <!-- row -->
                        <div class="row">
                            <!-- col -->
                            <div class="col-lg-12 col-md-6">
                                <div class="form-group">
                                    <label>Full Name</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Write Your Full Name">
                                    </div>
                                </div>                                
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-lg-12 col-md-6">
                                <div class="form-group">
                                    <label>Email</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Email">
                                    </div>
                                </div>                                
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-lg-12 col-md-6">
                                <div class="form-group">
                                    <label>Contact Number</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Contact Number">
                                    </div>
                                </div>                                
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-lg-12 col-md-6">
                                <div class="form-group">
                                    <label>Attach Resume</label>
                                    <div class="input-group">
                                        <input type="file" class="form-control" placeholder="Attach Resume">
                                    </div>
                                </div>                                
                            </div>
                            <!--/ col -->                             

                             <!-- col -->
                             <div class="col-lg-12">
                                <div class="form-group">
                                    <input type="submit" class="btn greenlink" value="SUBMIT">
                                </div>                                
                            </div>
                            <!--/ col -->                            
                        </div>
                        <!--/ row -->
                    </form>
                    <!--/ form -->
               </div>
               <!--/col -->
           </div>
           <!--/ row -->
        </div>
        <!--/ container -->

        

       
    </div>
    <!--/ sub page body -->
    
    </main>
    <!--/ main -->
    <!-- footer -->
   <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- script files -->
    <?php include 'scripts.php' ?>
    <!--/ script files -->
</body>
</html>