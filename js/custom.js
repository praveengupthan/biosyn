

$(document).ready(function(){
    $("#cat-dropdown-link").click(function(){
        $("#cat-items").slideToggle();
    });
});


    //add class to header on scroll
    $(window).scroll(function(){
        if($(this).scrollTop() > 30){
            $('.main-nav').addClass('fixed-top');
        }else{
            $('.main-nav').removeClass('fixed-top');
        }
    });

        //on click move to browser top
        $(document).ready(function(){
            $(window).scroll(function(){
                if($(this).scrollTop() > 100){
                    $('#movetop').fadeIn()
                }else{
                    $('#movetop').fadeOut();
                }
            });
    
            //click event to scroll to top
            $('#movetop').click(function(){
                $('html, body').animate({scrollTop:0}, 800);
            });
        });
    


//slick slider
$('.products-slick').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    centerMode: false,
    responsive: [{
            breakpoint: 1200,
            settings: {
                slidesToShow: 5,
                slidesToScroll: 1,
                infinite: true,
                dots: true
            }
        },
        {
            breakpoint: 991,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 766,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 574,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
    ]
});

    
    //home page text animation in slider
    
    $("#typed2").typing({
        strings: ['Photographers', 'Photo Studios', 'Digital Labs', 'Training Institute', 'Camera & Accessories', 'Sales & Services'],
        eraseDelay: 20,
        typeDelay: 100,
        stringStartDelay: 1000,
        color: 'white',
        typingColor: 'black',
        typingOpacity: '0.1',
        loopCount: 20,
        cursorBlink: true,
        cursorChar: '<small>_</small>',
        fade: true,
        onTyping: function () {
            console.log('onTyping');
        },
        onFinishedTyping: function () {
            console.log('onFinishedTyping');
        },
        onErasing: function () {
            console.log('onErasing');
        },
        onFinishedErasing: function () {
            console.log('onFinishedErasing');
        },
        onAllTypingCompleted: function () {
            console.log('onAllTypingCompleted');
        },
        onFinishedFadeErasing: function () {
            console.log('onFinishedFadeErasing');
        }
    });

    //tooltip
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })




    //slick slider
    $('.products-slick').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: true,
        centerMode: false,
        responsive: [{
                breakpoint: 1200,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 766,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 574,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });


 
