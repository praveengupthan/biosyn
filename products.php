<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Biosyn Chemicals Research Pvt Ltd</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- styles -->
   <?php include 'styles.php' ?>
</head>
<body>

    <!-- header -->
    <?php include 'header.php' ?>
    <!--/ header -->
    <!-- main -->
    <main class="subpage">

    <!-- sub page header -->
    <div class="subpage-header">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-6">
                    <article>
                        <h1>Products</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.php">Home</a></li>                                
                                <li class="breadcrumb-item active" aria-current="page">Products</li>
                            </ol>
                        </nav>
                    </article>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </div>
    <!--/ sub page header -->

    <!-- sub page body -->
    <div class="subpage-body">
        <!-- container -->
        <div class="container">

        <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-lg-3 col-md-3">
                <h2 class="section-title mb-4 aos-item" data-aos="fade-down">Our  <span>Categories</span> 
                    <a class="mob-dropdown-link" id="cat-dropdown-link" href="javascript:void(0)"> <span class="icon-bars icomoon"></span></a> </h2>
                    <ul class="cat-items aos-item" data-aos="fade-up" id="cat-items">
                        <li><a class="active-cat" href="javascript:void(0)">Acetophenones</a></li>
                        <li><a href="javascript:void(0)">benzophenones</a></li>
                        <li><a href="javascript:void(0)">Carbohydrates</a></li>
                        <li><a href="javascript:void(0)">Coumarins</a></li>
                        <li><a href="javascript:void(0)">Chalcones</a></li>
                        <li><a href="javascript:void(0)">Chromones & Chromanones</a></li>
                        <li><a href="javascript:void(0)">Flavones &amp; Flavanones</a></li>
                        <li><a href="javascript:void(0)">Natural Products</a></li>
                        <li><a href="javascript:void(0)">Fine Chemicals &amp; API Intermediates</a></li>
                        <li><a href="javascript:void(0)">Other Range of Products</a></li>
                        <li><a href="javascript:void(0)">Isocyanates &amp; Polyurethanes</a></li>  
                    </ul>
            </div>
            <!--/ col -->
            <!-- col -->
            <div class="col-lg-9 col-md-9">
                <!-- responsive table -->
                <div class="table-responsive aos-item" data-aos="fade-up">
                 <!-- table -->
                <table id="products-table" class="table table-bordered data-table" style="width:100%">
                    <thead>
                        <tr>
                            <th>CAS Number</th>
                            <th>Product Code</th>
                            <th>Product Name</th>
                            <th>Product Image</th>                   
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>42059-48-3</td>
                            <td>B1-101</td>
                            <td><a href="javascript:void(0)" data-toggle="modal" data-target="#product-more" class="fgreen">4’-Acetoxy-2’-hydroxy acetophenone</a></td>
                            <td class="text-center">
                                <img src="img/Acetophenones.png" alt="" class="data-tableimg">
                            </td>                   
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td>B1-102</td>
                            <td><a href="javascript:void(0)" data-toggle="modal" data-target="#product-more" class="fgreen">5’-Acetoxy-2’-hydroxy acetophenone</a></td>
                            <td class="text-center">
                                <img src="img/Benzophenones.png" alt="" class="data-tableimg">
                            </td>
                        
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td>B1-103</td>
                            <td><a href="javascript:void(0)" data-toggle="modal" data-target="#product-more" class="fgreen">3`-Benzyloxy-2`,4`-dimethoxy-6`-hydroxy acetophenone</a></td>
                            <td class="text-center">
                                <img src="img/Carbohydrates.png" alt="" class="data-tableimg">
                            </td>                   
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td>B1-104</td>
                            <td><a href="javascript:void(0)" data-toggle="modal" data-target="#product-more" class="fgreen">4’-Benzyloxy-2’-methoxyacetophenone</a></td>
                            <td class="text-center">
                                <img src="img/Chalcones.png" alt="" class="data-tableimg">
                            </td>                    
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td>B1-105</td>
                            <td><a href="javascript:void(0)" data-toggle="modal" data-target="#product-more" class="fgreen">4’-Benzyloxy acetophenone</a></td>
                            <td class="text-center">
                                <img src="img/Chromones-&-Chromanones.png" alt="" class="data-tableimg">
                            </td>                   
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td>B1-106</td>
                            <td><a href="javascript:void(0)" data-toggle="modal" data-target="#product-more" class="fgreen">2`-Benzoyloxy-4`-methoxy acetophenone</a></td>
                            <td class="text-center">
                                <img src="img/Coumarins.png" alt="" class="data-tableimg">
                            </td>                    
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td>B1-107</td>
                            <td><a href="javascript:void(0)" data-toggle="modal" data-target="#product-more" class="fgreen">3’-Bromo-5’-chloro-2’-hydroxyacetophenone</a></td>
                            <td class="text-center">
                                <img src="img/Fine-Chemicals-&-API-intermediates.png" alt="" class="data-tableimg">
                            </td>                   
                        </tr>

                        <tr>
                            <td>59443-15-1</td>
                            <td>B1-108</td>
                            <td><a href="javascript:void(0)" data-toggle="modal" data-target="#product-more" class="fgreen">5’-Bromo-3’-chloro-2’-hydroxyacetophenone</a></td>
                            <td class="text-center">
                                <img src="img/Flavones-&-Flavanones.png" alt="" class="data-tableimg">
                            </td>                   
                        </tr>

                        <tr>
                            <td>1204-21-3</td>
                            <td>B1-109</td>
                            <td><a href="javascript:void(0)" data-toggle="modal" data-target="#product-more" class="fgreen">2-Bromo-2’, 5’-dimethoxyacetophenone</a></td>
                            <td class="text-center">
                                <img src="img/Image-2.png" alt="" class="data-tableimg">
                            </td>
                        </tr>

                        <tr>
                            <td>1450-75-5	</td>
                            <td>B1-110</td>
                            <td><a href="javascript:void(0)" data-toggle="modal" data-target="#product-more" class="fgreen">5’-Bromo-2’-hydroxyacetophenone</a></td>
                            <td class="text-center">
                                <img src="img/Flavones-&-Flavanones.png" alt="" class="data-tableimg">
                            </td>
                            
                        </tr>

                        <tr>
                            <td>42059-48-3</td>
                            <td>B1-101</td>
                            <td><a href="javascript:void(0)" data-toggle="modal" data-target="#product-more" class="fgreen">4’-Acetoxy-2’-hydroxy acetophenone</a></td>
                            <td class="text-center">
                                <img src="img/Acetophenones.png" alt="" class="data-tableimg">
                            </td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td>B1-102</td>
                            <td><a href="javascript:void(0)" data-toggle="modal" data-target="#product-more" class="fgreen">5’-Acetoxy-2’-hydroxy acetophenone</a></td>
                            <td class="text-center">
                                <img src="img/Benzophenones.png" alt="" class="data-tableimg">
                            </td>                   
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td>B1-103</td>
                            <td><a href="javascript:void(0)" data-toggle="modal" data-target="#product-more" class="fgreen">3`-Benzyloxy-2`,4`-dimethoxy-6`-hydroxy acetophenone</a></td>
                            <td class="text-center">
                                <img src="img/Carbohydrates.png" alt="" class="data-tableimg">
                            </td>
                        
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td>B1-104</td>
                            <td><a href="javascript:void(0)" data-toggle="modal" data-target="#product-more" class="fgreen">4’-Benzyloxy-2’-methoxyacetophenone</a></td>
                            <td class="text-center">
                                <img src="img/Chalcones.png" alt="" class="data-tableimg">
                            </td>                  
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td>B1-105</td>
                            <td><a href="javascript:void(0)" data-toggle="modal" data-target="#product-more" class="fgreen">4’-Benzyloxy acetophenone</a></td>
                            <td class="text-center">
                                <img src="img/Chromones-&-Chromanones.png" alt="" class="data-tableimg">
                            </td>                   
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td>B1-106</td>
                            <td><a href="javascript:void(0)" data-toggle="modal" data-target="#product-more" class="fgreen">2`-Benzoyloxy-4`-methoxy acetophenone</a></td>
                            <td class="text-center">
                                <img src="img/Coumarins.png" alt="" class="data-tableimg">
                            </td>                    
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td>B1-107</td>
                            <td><a href="javascript:void(0)" data-toggle="modal" data-target="#product-more" class="fgreen">3’-Bromo-5’-chloro-2’-hydroxyacetophenone</a></td>
                            <td class="text-center">
                                <img src="img/Fine-Chemicals-&-API-intermediates.png" alt="" class="data-tableimg">
                            </td>                    
                        </tr>

                        <tr>
                            <td>59443-15-1</td>
                            <td>B1-108</td>
                            <td><a href="javascript:void(0)" data-toggle="modal" data-target="#product-more" class="fgreen">5’-Bromo-3’-chloro-2’-hydroxyacetophenone</a></td>
                            <td class="text-center">
                                <img src="img/Flavones-&-Flavanones.png" alt="" class="data-tableimg">
                            </td>                   
                        </tr>

                        <tr>
                            <td>1204-21-3</td>
                            <td>B1-109</td>
                            <td><a href="javascript:void(0)" data-toggle="modal" data-target="#product-more" class="fgreen">2-Bromo-2’, 5’-dimethoxyacetophenone</a></td>
                            <td class="text-center">
                                <img src="img/Image-2.png" alt="" class="data-tableimg">
                            </td>                   
                        </tr>

                        <tr>
                            <td>1450-75-5	</td>
                            <td>B1-110</td>
                            <td><a href="javascript:void(0)" data-toggle="modal" data-target="#product-more" class="fgreen">5’-Bromo-2’-hydroxyacetophenone</a></td>
                            <td class="text-center">
                                <img src="img/Flavones-&-Flavanones.png" alt="" class="data-tableimg">
                            </td>                   
                        </tr>
                    </tbody>           
                </table>
                <!--/ table -->
            </div>
            <!--/ col -->
        </div>
        <!--/ row -->



       

       

        
          
        </div>
        <!--/ container -->      
       
    </div>
    <!--/ sub page body -->
    
    </main>
    <!--/ main -->
    <!-- footer -->
   <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- script files -->
    <?php include 'scripts.php' ?>
    <!--/ script files -->

    
    <div class="modal fade" id="product-more" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel3" aria-hidden="true">
        <div class="modal-dialog modal-dialog-slideout modal-md" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">4’-Acetoxy-2’-hydroxy acetophenone</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <!-- modal body-->
            <div class="modal-body">
                <!-- details -->
                <div class="product-details">
                    <figure>
                        <img src="img/big-product.png" class="img-fluid w-100">
                    </figure>
                    <h5 class="fgreen fsemibold">Technical Data</h5>
                    <table class="table table-striped">
                        <tr>
                            <td>Category</td>
                            <td>:</td>
                            <td>Acetophenones</td>
                        </tr>
                        <tr>
                            <td>Product Code</td>
                            <td>:</td>
                            <td>FA03663</td>
                        </tr>
                        <tr>
                            <td>CAS No</td>
                            <td>:</td>
                            <td>98-86-2</td>
                        </tr>                       
                        <tr>
                            <td>MDL No</td>
                            <td>:</td>
                            <td>MFCD00008724</td>
                        </tr>
                        <tr>
                            <td>Chemical Formula</td>
                            <td>:</td>
                            <td>C8H8O</td>
                        </tr>
                        <tr>
                            <td>Molecular Weight</td>
                            <td>:</td>
                            <td>120.15</td>
                        </tr>
                        <tr>
                            <td>Appearance</td>
                            <td>:</td>
                            <td>Clear colourless to yellow liquid</td>
                        </tr>
                        <tr>
                            <td>Purity (GC)</td>
                            <td>:</td>
                            <td>min 99%</td>
                        </tr>
                    </table>
                    </div>
                    <!-- responsive table ends -->

                    <p>There is no hazardous surcharge associated with this product There is no special packaging charge associated with this product For research use only.  Not for human or veterinary use</p>
                </div>
                <!--/ details -->
              
            </div>         
            <!--/ modal body --> 
          </div>
        </div>
      </div>
</body>
</html>