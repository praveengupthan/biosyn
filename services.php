<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Biosyn Chemicals Research Pvt Ltd</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">

    <!-- styles -->
   <?php include 'styles.php' ?>
</head>

<body>

    <!-- header -->
    <?php include 'header.php' ?>
    <!--/ header -->
    <!-- main -->
    <main class="subpage">

    <!-- sub page header -->
    <div class="subpage-header">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-6">
                    <article>
                        <h1>Services</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>                                
                                <li class="breadcrumb-item active" aria-current="page">Services</li>
                            </ol>
                        </nav>
                    </article>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </div>
    <!--/ sub page header -->

    <!-- sub page body -->
    <div class="subpage-body">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row py-md-3">
                <!--col-->
                <div class="col-lg-6 col-md-6">
                    <img src="img/discovery-services.jpg" alt="" class="img-fluid">
                </div>
                <!--/ col -->

                 <!--col-->
                 <div class="col-lg-6 col-md-6 align-self-center aos-item" data-aos="fade-up">
                     <p class="fsemibold forange pb-0 mb-0">Synthetic Chemistry</p>
                    <h2 class="section-title">Discovery <span>Services</span></h2>
                    <p class="pb-0">Services offered in both FTE as well as FFS mode;</p>
                    <p>Synthesis of Research chemicals</p>
                    <ul class="list-items">
                        <li>Analogues</li>
                        <li>Intermediates</li>
                        <li>Building blocks</li>
                        <li>Library compounds</li>
                    </ul>
                 </div>
                <!--/ col -->
            </div>
            <!--/ row -->

             <!-- row -->
             <div class="row py-md-3">
                <!--col-->
                <div class="col-lg-6 col-md-6 order-md-last">
                    <img src="img/development-services.jpg" alt="" class="img-fluid">
                </div>
                <!--/ col -->

                 <!--col-->
                 <div class="col-lg-6 col-md-6 align-self-center aos-item" data-aos="fade-down">
                     <p class="fsemibold forange pb-0 mb-0">Synthetic Chemistry</p>
                    <h2 class="section-title">Development <span>Services</span></h2>
                    <p class="pb-0">Services offered in both FTE as well as FFS mode;</p>
                    <p>Synthesis of API intermediates</p>
                    <ul class="list-items">
                        <li>Fine chemicals</li>
                        <li>Scale-up of the customer’s process under secrecy agreement up to hundreds of kilograms</li>
                    </ul>
                 </div>
                <!--/ col -->
            </div>
            <!--/ row -->

             <!-- row -->
             <div class="row py-md-3">
                <!--col-->
                <div class="col-lg-6 col-md-6">
                    <img src="img/natural-products.jpg" alt="" class="img-fluid">
                </div>
                <!--/ col -->

                 <!--col-->
                 <div class="col-lg-6 col-md-6 align-self-center aos-item" data-aos="fade-up">                    
                    <h2 class="section-title">Natural <span>Products</span></h2>
                    <p class="pb-0">Servicing a global clients in more than 50 countries We’re a Leading Construction Company from Melbourne. Builiding Commerical and Non Commerical Buildings. Click on this video to check how we work with our clients and service providers.</p>                   
                 </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->

        <!-- quality control -->
        <div class="quality-control">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-lg-6">
                        <h2 class="h1">Quality <span>Control</span></h2>
                        <p>BRC believe in customer satisfaction through quality products delivery and Superior customer servic</p>
                        <p>BRC Quality control tests all incoming raw materials, in process materials and finished products against specifications. We are equipped with state-of-the-art equipment and systems that have been extensively audited by customers. </p>
                        <p>We have a team with excellent analytical capabilities in developing HPLC & GC methods for new products. We can provide NMR, IR, HPLC and GC data with each shipment at customer’s request. </p>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ quality control -->
    </div>
    <!--/ sub page body -->

    
        
        
    
    </main>
    <!--/ main -->
    <!-- footer -->
   <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- script files -->
    <?php include 'scripts.php' ?>
    <!--/ script files -->
</body>
</html>