<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Biosyn Chemicals Research Pvt Ltd</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- styles -->
   <?php include 'styles.php' ?>
</head>
<body>

    <!-- header -->
    <?php include 'header.php' ?>
    <!--/ header -->
    <!-- main -->
    <main class="subpage">

    <!-- sub page header -->
    <div class="subpage-header">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-6">
                    <article>
                        <h1>Research</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>                                
                                <li class="breadcrumb-item active" aria-current="page">Research</li>
                            </ol>
                        </nav>
                    </article>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </div>
    <!--/ sub page header -->

    <!-- sub page body -->
    <div class="subpage-body">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row py-md-3">
                <!--col-->
                <div class="col-lg-6 col-md-6">
                    <img src="img/research-development.jpg" alt="" class="img-fluid">
                </div>
                <!--/ col -->
                 <!--col-->
                 <div class="col-lg-6 col-md-6 align-self-center">                     
                    <h2 class="section-title aos-item" data-aos="fade-up">Resarch &amp;  <span>Development</span></h2>
                    <p class="aos-item" data-aos="fade-down">The BRC R&D department in BRC highly experienced Doctorate and Master Scientists who carry decades of Synthetic organic chemistry and analytical chemistry experience from multinational pharmaceutical companies. We also have senior doctorates in the form of strategic advisors. </p>

                    <p class="aos-item" data-aos="fade-up">This well-trained team is capable of delivering varied ranges of chemistry specialization coupled with extensive experience in the CRO and process R & D in delivering many projects and handling FTE collaborations. We undertake multi-step organic synthesis, supplying grams to multi Kgs. We have a solid reputation for our supply of building blocks and advanced intermediates. We can deliver target molecule with little or no literature precedent, we will investigate potential methods of preparation, optimization and scale-up, according to client needs.</p>                    
                 </div>
                <!--/ col -->
            </div>
            <!--/ row -->  
        </div>
        <!--/ container -->

        <!-- white section -->
        <div class="infrasection bgwhite py-4">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-lg-6 col-md-6">
                        <h2 class="section-title aos-item" data-aos="fade-up"><span>Infrastructure</span></h2>
                        <ul class="list-items aos-item" data-aos="fade-down">
                           <li>Total area: <span class="fbold fgreen">10,500 sq. ft.</span> </li>
                           <li>Fume hoods with N2, Air and Vacuum lines</li>
                           <li>Kilo lab with 20 L, 50 L and 100 L reactors</li>
                           <li>200 L to 6 KL on requirement basis (CDA signed facility)</li>
                           <li>1L, 5L, 20L Rotavaps</li>
                           <li>Hydrogenation lab with 250 mL to 2 L capacity</li>
                           <li>Autoclaves with 5L, 10L and 20 L capacity</li>
                           <li>Hot air vacuum ovens.</li>
                           <li>Column Chromatography</li>
                           <li>Lab space suitable for making mg to multi kg quantities</li>
                        </ul>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-6 col-md-6">
                        <img src="img/infraimg.png" alt="" class="img-fluid">
                    </div>
                    <!-- /col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ white section -->
    </div>
    <!--/ sub page body -->
    
    </main>
    <!--/ main -->
    <!-- footer -->
   <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- script files -->
    <?php include 'scripts.php' ?>
    <!--/ script files -->
</body>
</html>