<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/bootstrap.bundle.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>

<script>
$(document).ready(function() {
    $('#products-table').DataTable();
} );
</script>
<script src="js/popper.js"></script>
<script src="js/slick.js"></script>
<script src="js/aos.js"></script>
<script>
AOS.init({
    easing: 'ease-in-out-sine'
});
</script>
<script src="js/custom.js"></script>



