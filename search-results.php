<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Biosyn Chemicals Research Pvt Ltd</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- styles -->
   <?php include 'styles.php' ?>
</head>
<body>

    <!-- header -->
    <?php include 'header.php' ?>
    <!--/ header -->
    <!-- main -->
    <main class="subpage">

    <!-- sub page header -->
    <div class="subpage-header">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-6">
                    <article>
                        <h1>Search Results</h1>                       
                    </article>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </div>
    <!--/ sub page header -->

    <!-- sub page body -->
    <div class="subpage-body">
        <!-- container -->
        <div class="container">
        <p>Search Results for Cas Number: <span class="fbold fgreen">123456</span></p>

        <div class="row search-results">
            <!-- col -->
            <div class="col-lg-6">
                <!-- row -->
                <dl class="row">
                    <dt class="col-lg-4">Structure:</dt>
                    <dd class="col-lg-8"><img src="img/image-4.png"></dd> 
                </dl>   
                <!--/ row -->
                <!-- row -->
                <dl class="row">
                    <dt class="col-lg-4">Product Name:</dt>
                    <dd class="col-lg-8">4’-Acetoxy-2’-hydroxy acetophenone</dd> 
                </dl>   
                <!--/ row -->
                 <!-- row -->
                 <dl class="row">
                    <dt class="col-lg-4">Category:</dt>
                    <dd class="col-lg-8">ACETOPHENONES</dd> 
                </dl>   
                <!--/ row -->
                 <!-- row -->
                 <dl class="row">
                    <dt class="col-lg-4">Product Code:</dt>
                    <dd class="col-lg-8">B1-101</dd> 
                </dl>   
                <!--/ row -->
                 <!-- row -->
                 <dl class="row">
                    <dt class="col-lg-4">CAS No:</dt>
                    <dd class="col-lg-8">42059-48-3</dd> 
                </dl>   
                <!--/ row -->
                 <!-- row -->
                 <dl class="row">
                    <dt class="col-lg-4">MDL No:</dt>
                    <dd class="col-lg-8">MFCD00092225</dd> 
                </dl>   
                <!--/ row -->
                 <!-- row -->
                 <dl class="row">
                    <dt class="col-lg-4">Chemical Formula:</dt>
                    <dd class="col-lg-8">C10H10O4</dd> 
                </dl>   
                <!--/ row -->
                 <!-- row -->
                 <dl class="row">
                    <dt class="col-lg-4">Molecular Weight:</dt>
                    <dd class="col-lg-8">194.18</dd> 
                </dl>   
                <!--/ row -->
                 <!-- row -->
                 <dl class="row">
                    <dt class="col-lg-4">Appearance:</dt>
                    <dd class="col-lg-8">White to off white powder</dd> 
                </dl>   
                <!--/ row -->
                 <!-- row -->
                 <dl class="row">
                    <dt class="col-lg-4">Purity (GC):</dt>
                    <dd class="col-lg-8">98.00%</dd> 
                </dl>   
                <!--/ row -->
            </div>
            <!--/ col -->
        </div>

       

           
        </div>
        <!--/ container -->

       
    </div>
    <!--/ sub page body -->
    
    </main>
    <!--/ main -->
    <!-- footer -->
   <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- script files -->
    <?php include 'scripts.php' ?>
    <!--/ script files -->
</body>
</html>