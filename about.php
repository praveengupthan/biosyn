<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Biosyn Chemicals Research Pvt Ltd</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">

    <!-- styles -->
   <?php include 'styles.php' ?>
</head>

<body>

    <!-- header -->
    <?php include 'header.php' ?>
    <!--/ header -->
    <!-- main -->
    <main class="subpage">

    <!-- sub page header -->
    <div class="subpage-header">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-6">
                    <article>
                        <h1>About us</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>                                
                                <li class="breadcrumb-item active" aria-current="page">About us</li>
                            </ol>
                        </nav>
                    </article>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </div>
    <!--/ sub page header -->

    <!-- sub page body -->
    <div class="subpage-body">
        <!-- container -->
        <div class="container">
        <!--row -->
        <div class="row">
            <!-- col -->
            <div class="col-lg-8">
                <h2 class="section-title aos-item" data-aos="fade-up">About <span>BRC</span></h2>
                <p class="aos-item" data-aos="fade-down">Biosyn Research Chemicals, founded in 1998, is one of the leading service provider for Chemical development of Natural products, API intermediates and Heterocyclic compounds to Pharmaceutical, Biotechnology, Chemical industries and Agro companies across the globe. We provide both development support for Medchem groups, chemical development groups and Manufacturers. We have capabilities synthesize mgs to Kgs. The Services offered on FTE and FFS model.</p>
                <p class="aos-item" data-aos="fade-up">Biosyn with over 20 years of rich experience in the chemical development services with excellent problem solving skills. Our aim is to develop complex organic molecules and provide superior quality products on time, every time to help our clients reach their goals. We are committed to protection of IP, Confidentiality and Safety.  </p>
                <p class="aos-item" data-aos="fade-down">In addition, we have over 2600 in-house products including Natural products, Carbohydrates, Heterocyclics etc.  These need not be regarded as definitive and please enquire for your specific requirement. This list does not include the products synthesized under confidentially agreement and we are always happy to enter into manufacture under such agreements.</p>

                <h2 class="section-title aos-item" data-aos="fade-up">Our <span>Mission</span></h2>
                <p class="aos-item" data-aos="fade-down">Our mission is to become a Leading manufacturer in the global market with Science, Innovation & Commitment to develop complex organic molecules and provide superior quality products on time, every time to help our clients reach their goals</p>
            </div>
            <!-- /col -->

            <!-- col -->
            <div class="col-lg-4">
                <h2 class="section-title aos-item" data-aos="fade-down">Why <span>Biosyn</span></h2>
                <ul class="list-items aos-item" data-aos="fade-up">
                    <li>Respect for Intellectual property rights</li>
                    <li>Quick Response Time</li>
                    <li>Cost effective solutions</li>
                    <li>Quality products</li>
                    <li>Product delivery on time</li>
                    <li>Project management</li>
                    <li>Team of qualified employees</li>
                    <li>Respecting confidentiality</li>
                    <li>Ethical business practice</li>
                </ul>
            </div>
            <!--/ col -->
        </div>
        <!--/ row -->
           
        </div>
        <!--/ container -->
    </div>
    <!--/ sub page body -->

    
        
        
    
    </main>
    <!--/ main -->
    <!-- footer -->
   <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- script files -->
    <?php include 'scripts.php' ?>
    <!--/ script files -->
</body>
</html>