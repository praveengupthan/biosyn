<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Biosyn Chemicals Research Pvt Ltd</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- styles -->
   <?php include 'styles.php' ?>
</head>
<body>

    <!-- header -->
    <?php include 'header.php' ?>
    <!--/ header -->
    <!-- main -->
    <main class="subpage">

    <!-- sub page header -->
    <div class="subpage-header">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-6">
                    <article>
                        <h1>Enquiry</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>                                
                                <li class="breadcrumb-item active" aria-current="page">Enquiry</li>
                            </ol>
                        </nav>
                    </article>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </div>
    <!--/ sub page header -->

    <!-- sub page body -->
    <div class="subpage-body">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-4">
                    <h3 class="h5 fbold fgreen pb-3">Frequently Asked Questions</h3>
                    <!-- div -->
                    <div class="py-1 mb-1 border-bottom">
                        <h6 class="fbold">Lorem Ipsum is simply dummy text of the printing?</h6>
                        <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters.</p>
                    </div>
                    <!-- /div -->

                     <!-- div -->
                     <div class="py-1 mb-1 border-bottom">
                        <h6 class="fbold">Lorem Ipsum is simply dummy text of the printing?</h6>
                        <p>It is a long established fact that a reader will be distracted by the readable content.</p>
                    </div>
                    <!-- /div -->

                     <!-- div -->
                     <div class="py-1 mb-1 border-bottom">
                        <h6 class="fbold">Lorem Ipsum is simply dummy text of the printing?</h6>
                        <p>All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable.</p>
                    </div>
                    <!-- /div -->

                      <!-- div -->
                      <div class="py-1 mb-1 border-bottom">
                        <h6 class="fbold">Lorem Ipsum is simply dummy text of the printing?</h6>
                        <p>It is a long established fact that a reader will be distracted by the readable content.</p>
                    </div>
                    <!-- /div -->

                    
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-8">
                    <h2 class="section-title">Enquiry <span>Now</span></h2>
                    <p>Send the following details, our Team will contact you shortly</p>

                    <!-- form -->
                    <form>
                        <!-- row -->
                        <div class="row">
                            <!-- col -->
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <label>Name</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Write Your Name">
                                    </div>
                                </div>                                
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <label>Company Name</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Company Name">
                                    </div>
                                </div>                                
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <label>Designation</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Ex: Asst Manager">
                                    </div>
                                </div>                                
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <label>Address Line 01</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Ex: House No / Plot Number/ Street Number">
                                    </div>
                                </div>                                
                            </div>
                            <!--/ col -->
                             <!-- col -->
                             <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <label>Address Line 02</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Ex: Area Name / Town">
                                    </div>
                                </div>                                
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <label>City</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="City Name">
                                    </div>
                                </div>                                
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <label>Postal Code</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Postal Code">
                                    </div>
                                </div>                                
                            </div>
                            <!--/ col -->
                             <!-- col -->
                             <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <label>State</label>
                                    <div class="input-group">
                                        <select class="form-control">
                                            <option>Select State</option>
                                            <option>Andhra Pradesh</option>
                                            <option>Telangana</option>
                                        </select>
                                    </div>
                                </div>                                
                            </div>
                            <!--/ col -->
                             <!-- col -->
                             <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <label>Contact Phone Number</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Mobile / Landline Number">
                                    </div>
                                </div>                                
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <label>Email</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Email Official / Personal">
                                    </div>
                                </div>                                
                            </div>
                            <!--/ col -->
                             <!-- col -->
                             <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Enquiry Message</label>
                                    <div class="input-group">
                                        <textarea class="form-control" style="height:100px" placeholder="Write Message"></textarea>
                                    </div>
                                </div>                                
                            </div>
                            <!--/ col -->

                             <!-- col -->
                             <div class="col-lg-12">
                                <div class="form-group">
                                    <input type="submit" class="btn greenlink" value="SUBMIT">
                                </div>                                
                            </div>
                            <!--/ col -->                            
                        </div>
                        <!--/ row -->
                    </form>
                    <!--/ form -->
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->  
        </div>
        <!--/ container -->       
       
    </div>
    <!--/ sub page body -->
    
    </main>
    <!--/ main -->
    <!-- footer -->
   <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- script files -->
    <?php include 'scripts.php' ?>
    <!--/ script files -->
</body>
</html>