<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Biosyn Chemicals Research Pvt Ltd</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- styles -->
   <?php include 'styles.php' ?>
</head>
<body>

    <!-- header -->
    <?php include 'header.php' ?>
    <!--/ header -->
    <!-- main -->
    <main class="subpage">

    <!-- sub page header -->
    <div class="subpage-header">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-6">
                    <article>
                        <h1>Contact</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>                                
                                <li class="breadcrumb-item active" aria-current="page">Contact</li>
                            </ol>
                        </nav>
                    </article>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </div>
    <!--/ sub page header -->

    <!-- sub page body -->
    <div class="subpage-body">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-4 col-md-4">
                    <h2 class="section-title pb-3">Contact <span>Details</span></h2>
                    <!-- div -->
                    <div class="d-flex py-1 mb-1 border-bottom aos-item" data-aos="fade-up">
                        <div class="pt-1">
                            <span class="icon-pin icomoon"></span>
                        </div>
                        <div class="pl-3">
                            <p class="text-left">Plot No-176/A, Phase-II, IDA, Mallapur, Hyderabad, Telangana 500076</p>
                        </div>
                    </div>
                    <!-- /div -->

                     <!-- div -->
                     <div class="d-flex py-1 mb-1 border-bottom aos-item" data-aos="fade-down">
                        <div class="pt-1">
                            <span class="icon-phone icomoon"></span>
                        </div>
                        <div class="pl-3">
                            <p class="text-left pb-0 mb-0">+91 40 2717 8860</p>
                            <p class="text-left">+91 9347 256 589</p>
                        </div>                        
                    </div>
                    <!-- /div -->

                     <!-- div -->
                     <div class="d-flex py-1 mb-1 border-bottom aos-item" data-aos="fade-up">
                        <div class="pt-1">
                            <span class="icon-www icomoon"></span>
                        </div>
                        <div class="pl-3">
                            <p class="text-left pb-0 mb-0">info@biosyn.in</p>
                            <p class="text-left">www.biosyn.in</p>
                        </div>                        
                    </div>
                    <!-- /div -->

                     <!-- div -->
                     <div class="d-flex py-1 mb-1 border-bottom aos-item" data-aos="fade-down">
                        <div class="pt-1">
                            <span class="icon-clock icomoon"></span>
                        </div>
                        <div class="pl-3">
                            <p class="text-left pb-0 mb-0">Monday - Friday 9 AM - 7PM</p>
                            <p class="text-left">Saturday  9 AM - 5PM</p>
                        </div>                        
                    </div>
                    <!-- /div -->
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-8 col-md-8 aos-item" data-aos="fade-up">
                    <h2 class="section-title">Reach <span>Us</span></h2>
                    <p>Send the following details, our Team will contact you shortly</p>

                    <!-- form -->
                    <form>
                        <!-- row -->
                        <div class="row">
                            <!-- col -->
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Full Name</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Write Your Full Name">
                                    </div>
                                </div>                                
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Email</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Email">
                                    </div>
                                </div>                                
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Contact Number</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Contact Number">
                                    </div>
                                </div>                                
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Subject</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Subject">
                                    </div>
                                </div>                                
                            </div>
                            <!--/ col -->
                             <!-- col -->
                             <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Message</label>
                                    <div class="input-group">
                                        <textarea class="form-control" placeholder="Write Message"></textarea>
                                    </div>
                                </div>                                
                            </div>
                            <!--/ col -->

                             <!-- col -->
                             <div class="col-lg-12">
                                <div class="form-group">
                                    <input type="submit" class="btn greenlink" value="SUBMIT">
                                </div>                                
                            </div>
                            <!--/ col -->                            
                        </div>
                        <!--/ row -->
                    </form>
                    <!--/ form -->
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-12">
                    <!-- map-->
                    <div class="map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d15220.162064645885!2d78.41834594999999!3d17.50557925!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1588071105160!5m2!1sen!2sin" width="100%" height="350" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                    </div>
                    <!--/ map -->
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->  
        </div>
        <!--/ container -->

        

       
    </div>
    <!--/ sub page body -->
    
    </main>
    <!--/ main -->
    <!-- footer -->
   <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- script files -->
    <?php include 'scripts.php' ?>
    <!--/ script files -->
</body>
</html>