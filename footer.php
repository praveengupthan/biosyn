<footer>
        <!--top footer -->
        <div class="top-footer">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-lg-4 d-none d-md-block">
                        <p class="pb-4">Biosyn is highly focused on developing strategy for multi product activity. We have the widest range of basic intermediates and building blocks to fasten the discovery of bio-active molecules. </p>
                        <a class="greenlink" href="about.php">Read More</a>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-md-2 col-5">
                        <div class="footer-nav">
                            <h5>Company</h5>
                            <ul>
                                <li>
                                    <a href="index.php">Home</a>
                                </li>
                                <li>
                                    <a href="about.php">About us</a>
                                </li>
                                <li>
                                    <a href="products.php">Products</a>
                                </li>
                                <li>
                                    <a href="services.php">Services</a>
                                </li>
                                <li>
                                    <a href="research.php">Research</a>
                                </li>
                                <li>
                                    <a href="careers.php">Careers</a>
                                </li>
                                <li>
                                    <a href="contact.php">Contact</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--/col-->                   

                    <!-- col -->
                    <div class="col-md-3 col-7">
                        <div class="footer-nav">
                            <h5>Contact</h5>
                            <p><span class="icon-phone icomoon"></span> +844 1800 33 555</p>
                            <p><span class="icon-envelope icomoon"></span> info@biosyn.in</p>

                            <ul class="border-top footer-social pt-3 nav">
                                <li class="nav-item"><a class="nav-link" href="javascript:void(0)" target="_blank"><span class="icon-facebook icomoon"></span></a></li>
                                <li class="nav-item"><a class="nav-link" href="javascript:void(0)" target="_blank"> <span class="icon-twitter icomoon"></span></a></li>
                                <li class="nav-item"><a class="nav-link" href="javascript:void(0)" target="_blank">  <span class="icon-linkedin icomoon"></span></a></li>
                            </ul>
                        </div>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--container -->
        </div>
        <!--/ top footer -->
         <!--bottom footer -->
         <div class="bot-footer text-center">
             <p>Copyright © BIO SYN 2020</p>
         </div>
         <!--/ bottom footer -->
         <a href="javascript:void(0)" id="movetop"><span class="icon-arrow-up icomoon"></span></a>
    </footer>