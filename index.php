<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Biosyn Chemicals Research Pvt Ltd</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- styles -->
   <?php include 'styles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'header.php' ?>
    <!--/ header -->

    <!-- main -->
    <main>
        <!-- slider -->
    <div data-ride="carousel" class="carousel carousel-fade" id="carousel-example-captions">
        <ol class="carousel-indicators">
            <li class="active" data-slide-to="0" data-target="#carousel-example-captions"></li>
            <li data-slide-to="1" data-target="#carousel-example-captions" class=""></li>
            <li data-slide-to="2" data-target="#carousel-example-captions" class=""></li>
            <li data-slide-to="3" data-target="#carousel-example-captions" class=""></li>
        </ol>
        <div role="listbox" class="carousel-inner">
            <div class="carousel-item active">
                <img class="slider-img-opacity d-none d-sm-block" src="img/slider01-opacityimg.svg" alt="">
                <div class="container">
                    <div class="carousel-caption vertical-center">
                        <h3>Biomedical products:</h3>
                        <ul>
                            <li>PEG derivatives</li>
                            <li>Isocyanates</li>
                            <li>Polyurethanes</li>
                            <li>Photoinitiators</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
            <img class="slider-img-opacity d-none d-sm-block" src="img/slider02-opacityimg.svg" alt="">
                <div class="container">
                    <div class="carousel-caption">
                        <h3>Natural Products:</h3>
                        <ul>
                            <li>Beta-Cabolines</li>
                            <li>Alkaloids</li>
                            <li>Naphthoquinones</li>
                            <li>Tri terpenoids</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
            <img class="slider-img-opacity d-none d-sm-block" src="img/slider03-opacityimg.svg" alt="">
                <div class="container">
                    <div class="carousel-caption">
                        <h3>Custom synthesis:</h3>
                        <ul>
                            <li>Research chemicals</li>
                            <li>API intermediates</li>
                            <li>Library compounds</li>
                            <li>Scaffolds and building blocks</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="container">
                    <div class="carousel-caption">
                        <h3>Range of in house Products:</h3>
                        <ul>
                            <li>Acetophenones and benzophenones</li>
                            <li>Carbohydrates</li>
                            <li>Chromones and Chromanones</li>
                            <li>Flavones and flavanones</li>
                            <li>Heterocyclics</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>       
    </div>
    <!-- slider -->

    <!-- categories -->
    <div class="categories">
        <div class="formulatrans d-none d-md-block"><img src="img/formula.png" alt=""></div>
        <div class="microscope d-none d-md-block"><img src="img/microscope.png" alt=""></div>
        <!-- container  -->
        <div class="container">
            <!-- categories -->
            <div class="categories-items">
                <div class="cat-title"><img src="img/CATEGORIES.png" alt=""></div>
               <!-- row -->
               <div class="row">
                   <!-- col-->
                   <div class="col-lg-4 col-md-6">
                        <!-- items -->
                        <div class="cat-item d-flex aos-item" data-aos="fade-up">
                            <div class="cat-figure">
                                <a href="products.php"><img src="img/Acetophenones.png" alt=""></a>
                            </div>
                            <article>
                                <h5>
                                    <a href="products.php">ACETOPHENONES</a>
                                </h5>
                                <p>Aromatic ketone consisting of (C=O)CH3, and its substituted derivatives</p>
                            </article>                            
                        </div>
                        <!--/ items -->
                   </div>
                   <!--/ col -->

                    <!-- col-->
                    <div class="col-lg-4 col-md-6">
                        <!-- items -->
                        <div class="cat-item d-flex aos-item" data-aos="fade-down">
                            <div class="cat-figure">
                                <a href="products.php"><img src="img/Benzophenones.png" alt=""></a>
                            </div>
                            <article>
                                <h5>
                                    <a href="products.php">BENZOPHENONES</a>
                                </h5>
                                <p>Aromatic ketone consisting of (C=O)Ph, and its substituted derivatives</p>
                            </article>                            
                        </div>
                        <!--/ items -->
                   </div>
                   <!--/ col -->

                    <!-- col-->
                    <div class="col-lg-4 col-md-6">
                        <!-- items -->
                        <div class="cat-item d-flex aos-item" data-aos="fade-up">
                            <div class="cat-figure">
                                <a href="products.php"><img src="img/Carbohydrates.png" alt=""></a>
                            </div>
                            <article>
                                <h5>
                                    <a href="products.php">CARBOHYDRATES</a>
                                </h5>
                                <p>Compounds with hydrogen–oxygen atom ratio of 2:1 (as in water) and with the empirical formula Cm(H2O)n</p>
                            </article>                            
                        </div>
                        <!--/ items -->
                   </div>
                   <!--/ col -->

                    <!-- col-->
                    <div class="col-lg-4 col-md-6">
                        <!-- items -->
                        <div class="cat-item d-flex aos-item" data-aos="fade-down">
                            <div class="cat-figure">
                                <a href="products.php"><img src="img/Coumarins.png" alt=""></a>
                            </div>
                            <article>
                                <h5>
                                    <a href="products.php">COUMARINS</a>
                                </h5>
                                <p>benzopyrone class of organic compounds found in many plants</p>
                            </article>                            
                        </div>
                        <!--/ items -->
                   </div>
                   <!--/ col -->

                    <!-- col-->
                    <div class="col-lg-4 col-md-6">
                        <!-- items -->
                        <div class="cat-item d-flex aos-item" data-aos="fade-up">
                            <div class="cat-figure">
                                <a href="products.php"><img src="img/Chalcones.png" alt=""></a>
                            </div>
                            <article>
                                <h5>
                                    <a href="products.php">CHALCONES</a>
                                </h5>
                                <p>Promising synthons and bioactive scaffolds of great medicinal interest</p>
                            </article>                            
                        </div>
                        <!--/ items -->
                   </div>
                   <!--/ col -->

                    <!-- col-->
                    <div class="col-lg-4 col-md-6">
                        <!-- items -->
                        <div class="cat-item d-flex aos-item" data-aos="fade-down">
                            <div class="cat-figure">
                                <a href="products.php"><img src="img/Chromones-&-Chromanones.png" alt=""></a>
                            </div>
                            <article>
                                <h5>
                                    <a href="products.php">CHROMONES </a>
                                </h5>
                                <p>hetrocyclic compounds with benzopyron network with substituted keto group on pyron ring</p>
                            </article>                            
                        </div>
                        <!--/ items -->
                   </div>
                   <!--/ col -->

                    <!-- col-->
                    <div class="col-lg-4 col-md-6">
                        <!-- items -->
                        <div class="cat-item d-flex aos-item" data-aos="fade-up">
                            <div class="cat-figure">
                                <a href="products.php"><img src="img/Flavones-&-Flavanones.png" alt=""></a>
                            </div>
                            <article>
                                <h5>
                                    <a href="products.php">FLAVONES & FLAVANONES</a>
                                </h5>
                                <p>Natural products of the benzopyran class constituting an important group of oxygen heterocycles</p>
                            </article>                            
                        </div>
                        <!--/ items -->
                   </div>
                   <!--/ col -->

                    <!-- col-->
                    <div class="col-lg-4 col-md-6">
                        <!-- items -->
                        <div class="cat-item d-flex aos-item" data-aos="fade-down">
                            <div class="cat-figure">
                                <a href="products.php"><img src="img/Natural-products.png" alt=""></a>
                            </div>
                            <article>
                                <h5>
                                    <a href="products.php">NATURAL PRODUCTS</a>
                                </h5>
                                <p>Small molecules produced naturally by any organism including primary and secondary metabolites</p>
                            </article>                            
                        </div>
                        <!--/ items -->
                   </div>
                   <!--/ col -->

                    <!-- col-->
                    <div class="col-lg-4 col-md-6">
                        <!-- items -->
                        <div class="cat-item d-flex aos-item" data-aos="fade-up">
                            <div class="cat-figure">
                                <a href="products.php"><img src="img/Fine-Chemicals-&-API-intermediates.png" alt=""></a>
                            </div>
                            <article>
                                <h5>
                                    <a href="products.php">FINE CHEMICALS & API INTERMEDIATES</a>
                                </h5>
                                <p>Complex, single, pure chemical substances, produced in limited quantities</p>
                            </article>                            
                        </div>
                        <!--/ items -->
                   </div>
                   <!--/ col -->

                    <!-- col-->
                    <div class="col-lg-4 col-md-6">
                        <!-- items -->
                        <div class="cat-item d-flex aos-item" data-aos="fade-down">
                            <div class="cat-figure">
                                <a href="products.php"><img src="img/Other-Range-of-products.png" alt=""></a>
                            </div>
                            <article>
                                <h5>
                                    <a href="products.php">OTHER RANGE OF PRODUCTS</a>
                                </h5>
                                <p>Aromatics, Oxygen and Nitrogen heterocycles etc.,</p>
                            </article>                            
                        </div>
                        <!--/ items -->
                   </div>
                   <!--/ col -->

                    <!-- col-->
                    <div class="col-lg-4 col-md-6">
                        <!-- items -->
                        <div class="cat-item d-flex aos-item" data-aos="fade-up">
                            <div class="cat-figure">
                                <a href="products.php"><img src="img/Isocyanates-&-Polyurethanes.png" alt=""></a>
                            </div>
                            <article>
                                <h5>
                                    <a href="products.php">ISOCYANATES &  POLYURETHANES</a>
                                </h5>
                                <p>Products used in biomedical engineering</p>
                            </article>                            
                        </div>
                        <!--/ items -->
                   </div>
                   <!--/ col -->
               </div>
               <!--/ row -->
            </div>
            <!--/ categories -->

            <!-- papular products-->
            <div class="popular-products">
                <div class="cat-title"><img src="img/PRODUCTS.png" alt=""></div>

                <div class="title-block">
                    <h3>Popular <span>Products</span></h3>
                </div>

                <!--slick -->
                <!-- row -->
                <div class="row products-slick">
                    <div class="slide col-sm-3 col-6 text-center" data-toggle="modal" data-target="#product-more">
                        <a href="javascript:void(0)"><img src="img/Image-2.png" alt="" class="img-fluid" /></a>
                        <p class="text-center border-bottom pb-2">4’-Acetoxy-2’-hydroxy acetophenone</p>
                        <p class="text-center pt-2">Code: B1-101</p>
                    </div>
                    <div class="slide col-sm-3 col-6 text-center">
                        <a href="javascript:void(0)"><img src="img/Image-3.png" alt="" class="img-fluid" /></a>
                        <p class="text-center border-bottom pb-2">5’-Acetoxy-2’-hydroxy acetophenone</p>
                        <p class="text-center pt-2">Code: B1-102</p>
                    </div>
                    <div class="slide col-sm-3 col-6 text-center">
                        <a href="javascript:void(0)"><img src="img/Image-4.png" alt="" class="img-fluid" /></a>
                        <p class="text-center border-bottom pb-2">3`-Benzyloxy-2`,4`-dimethoxy-6`-hydroxy acetophenone</p>
                        <p class="text-center pt-2">Code: B1-103</p>
                    </div>
                    <div class="slide col-sm-3 col-6 text-center">
                        <a href="javascript:void(0)"><img src="img/Image-5.png" alt="" class="img-fluid" /></a>
                        <p class="text-center border-bottom pb-2">4’-Benzyloxy-2’-methoxyacetophenone</p>
                        <p class="text-center pt-2">Code: B1-104</p>
                    </div>
                    <div class="slide col-sm-3 col-6 text-center">
                        <a href="javascript:void(0)"><img src="img/Image-2.png" alt="" class="img-fluid" /></a>
                        <p class="text-center border-bottom pb-2">4’-Acetoxy-2’-hydroxy acetophenone</p>
                        <p class="text-center pt-2">Code: B1-101</p>
                    </div>
                    <div class="slide col-sm-3 col-6 text-center">
                        <a href="javascript:void(0)"><img src="img/Image-3.png" alt="" class="img-fluid" /></a>
                        <p class="text-center border-bottom pb-2">5’-Acetoxy-2’-hydroxy acetophenone</p>
                        <p class="text-center pt-2">Code: B1-102</p>
                    </div>
                    <div class="slide col-sm-3 col-6 text-center">
                        <a href="javascript:void(0)"><img src="img/Image-4.png" alt="" class="img-fluid" /></a>
                        <p class="text-center border-bottom pb-2">3`-Benzyloxy-2`,4`-dimethoxy-6`-hydroxy acetophenone</p>
                        <p class="text-center pt-2">Code: B1-103</p>
                    </div>
                    <div class="slide col-sm-3 col-6 text-center">
                        <a href="javascript:void(0)"> <img src="img/Image-5.png" alt="" class="img-fluid" /></a>
                        <p class="text-center border-bottom pb-2">4’-Benzyloxy-2’-methoxyacetophenone</p>
                        <p class="text-center pt-2">Code: B1-104</p>
                    </div>                    
                </div>
                <!--/ row -->
                <!--/ slick -->
            </div>
            <!--/ papular products -->            
        </div>
        <!--/ container  -->
    </div>
    <!--/ categories -->
    </main>
    <!--/ main -->
    <!-- footer -->
   <?php include 'footer.php' ?>
    <!--/ footer -->    

    <!-- script files -->
    <?php include 'scripts.php' ?>
    <!--/ script files -->
    <div class="modal fade" id="product-more" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel3" aria-hidden="true">
        <div class="modal-dialog modal-dialog-slideout modal-md" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">4’-Acetoxy-2’-hydroxy acetophenone</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <!-- modal body-->
            <div class="modal-body">
                <!-- details -->
                <div class="product-details">
                    <figure>
                        <img src="img/big-product.png" class="img-fluid w-100">
                    </figure>
                    <h5 class="fgreen fsemibold">Technical Data</h5>
                    <table class="table table-striped">
                        <tr>
                            <td>Category</td>
                            <td>:</td>
                            <td>Acetophenones</td>
                        </tr>
                        <tr>
                            <td>CAS No</td>
                            <td>:</td>
                            <td>98-86-2</td>
                        </tr>
                        <tr>
                            <td>Product Code</td>
                            <td>:</td>
                            <td>FA03663</td>
                        </tr>
                        <tr>
                            <td>MDL No</td>
                            <td>:</td>
                            <td>MFCD00008724</td>
                        </tr>
                        <tr>
                            <td>Chemical Formula</td>
                            <td>:</td>
                            <td>C8H8O</td>
                        </tr>
                        <tr>
                            <td>Molecular Weight</td>
                            <td>:</td>
                            <td>120.15</td>
                        </tr>
                        <tr>
                            <td>Appearance</td>
                            <td>:</td>
                            <td>Clear colourless to yellow liquid</td>
                        </tr>
                        <tr>
                            <td>Purity (GC)</td>
                            <td>:</td>
                            <td>min 99%</td>
                        </tr>
                    </table>

                    <p>There is no hazardous surcharge associated with this product There is no special packaging charge associated with this product For research use only.  Not for human or veterinary use</p>
                </div>
                <!--/ details -->              
            </div>         
            <!--/ modal body --> 
          </div>
        </div>
      </div>
</body>
</html>