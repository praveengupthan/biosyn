
<header>
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row py-sm-2">
                <!-- col -->
                <div class="col-sm-3 col-md-3">
                    <a href="index.php" class="brand-logo">
                        <img src="img/logo.svg" class="brand-img" alt="">
                    </a>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-sm-9 col-md-6  align-self-center d-none d-sm-block">
                    <!-- header search -->
                    <div class="header-search d-flex justify-content-center">

                        <input type="text" placeholder="Search  CAS No" list="searchCas" name="SearchCas" >
                        <datalist id="searchCas" class="dataList" style="overflow-y: auto!important">
                            <option value="832-58-6"></option>
                            <option value="13909-73-4"></option>
                            <option value="2161-85-5"></option>
                            <option value="832-58-6"></option>
                            <option value="15485-66-2"></option>
                            <option value="2491-31-8"></option>
                            <option value="66108-30-3"></option>
                            <option value="832-58-6"></option>
                            <option value="13909-73-4"></option>
                            <option value="2161-85-5"></option>
                          
                            
                        </datalist>
                        <button onclick="window.location.href='search-results.php';"><span class="icon-search icomoon"></span></button>
                    </div>
                    <!--/ header search -->
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-sm-12 col-md-3 col-6 text-sm-right header-social align-self-center d-none d-md-block">
                    <a href="javascript:void(0)" target="_blank">
                        <span class="icon-facebook icomoon"></span>
                    </a>
                    <a href="javascript:void(0)" target="_blank">
                        <span class="icon-twitter icomoon"></span>
                    </a>
                    <a href="javascript:void(0)" target="_blank">
                        <span class="icon-linkedin icomoon"></span>
                    </a>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
        <!-- main nav bar -->
        <div class="main-nav">
            <!-- container -->
            <div class="container px-0">
                <nav class="navbar navbar-expand-md navbar-light">                   
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icon-bars icomoon"></span>
                    </button>
                  
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                      <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                          <a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='index.php'){echo'active';}else {echo'nav-link';} ?>" href="index.php">Home</a>
                        </li>
                        <li class="nav-item">
                          <a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='about.php'){echo'active';}else {echo'nav-link';} ?>" href="about.php">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='research.php'){echo'active';}else {echo'nav-link';} ?> " href="research.php">Research</a>
                        </li>
                      
                        <li class="nav-item dropdown">
                          <a class="nav-link dropdown-toggle <?php if(
                            basename($_SERVER[ 'SCRIPT_NAME'])=='products.php' || 
                            basename($_SERVER[ 'SCRIPT_NAME'])=='products.php' || 
                            basename($_SERVER[ 'SCRIPT_NAME'])=='products.php' || 
                            basename($_SERVER[ 'SCRIPT_NAME'])=='products.php'||
                            basename($_SERVER[ 'SCRIPT_NAME'])=='products.php'||
                            basename($_SERVER[ 'SCRIPT_NAME'])=='products.php'||
                            basename($_SERVER[ 'SCRIPT_NAME'])=='products.php'||
                            basename($_SERVER[ 'SCRIPT_NAME'])=='products.php'||
                            basename($_SERVER[ 'SCRIPT_NAME'])=='products.php'||
                            basename($_SERVER[ 'SCRIPT_NAME'])=='products.php'||
                            basename($_SERVER[ 'SCRIPT_NAME'])=='products.php'
                            ) {echo 'active'; } else {echo 'nav-link'; }?>  " href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Categories
                          </a>
                          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'products.php'){echo 'active-subnav'; }else { echo 'dropdown-item'; } ?>" href="products.php">Acetophenones</a>
                            <a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'products.php'){echo 'active-subnav'; }else { echo 'dropdown-item'; } ?>" href="products.php">benzophenones</a>                            
                            <a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'products.php'){echo 'active-subnav'; }else { echo 'dropdown-item'; } ?>" href="products.php">Carbohydrates</a>
                            <a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'products.php'){echo 'active-subnav'; }else { echo 'dropdown-item'; } ?>" href="products.php">Coumarins</a>
                            <a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'products.php'){echo 'active-subnav'; }else { echo 'dropdown-item'; } ?>" href="products.php">Chalcones</a>                            
                            <a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'products.php'){echo 'active-subnav'; }else { echo 'dropdown-item'; } ?>" href="products.php">Chromones & Chromanones</a>
                            <a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'products.php'){echo 'active-subnav'; }else { echo 'dropdown-item'; } ?>" href="products.php">Flavones &amp; Flavanones</a>
                            <a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'products.php'){echo 'active-subnav'; }else { echo 'dropdown-item'; } ?>" href="products.php">Natural Products</a>                            
                            <a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'products.php'){echo 'active-subnav'; }else { echo 'dropdown-item'; } ?>" href="products.php">Fine Chemicals &amp; API Intermediates</a>
                            <a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'products.php'){echo 'active-subnav'; }else { echo 'dropdown-item'; } ?>" href="products.php">Other Range of Products</a>
                            <a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'products.php'){echo 'active-subnav'; }else { echo 'dropdown-item'; } ?>" href="products.php">Isocyanates &amp; Polyurethanes</a>                            
                            
                          </div>
                        </li>
                        <li class="nav-item">
                          <a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='services.php'){echo'active';}else {echo'nav-link';} ?> " href="services.php">Services</a>
                        </li>
                        
                        <li class="nav-item">
                            <a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='careers.php'){echo'active';}else {echo'nav-link';} ?> " href="careers.php">Careers</a>
                        </li>
                        <li class="nav-item">
                            <a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='contact.php'){echo'active';}else {echo'nav-link';} ?> " href="contact.php">Contact</a>
                        </li>
                      </ul>
                      <div class="my-2 my-lg-0 rt-link">                       
                        <a href="enquiry.php" class="btn d-inline-block my-2 my-sm-0 text-uppercase">Enquiry Now</a>
                      </div>
                    </div>
                  </nav>
            </div>
            <!--/ container -->
        </div>
        <!--/ main nav bar -->
    </header>